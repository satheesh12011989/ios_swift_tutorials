//
//  ViewController.h
//  TouchID
//
//  Created by Satheesh  on 8/12/14.
//  Copyright (c) 2014 Satheesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *authentication;

- (IBAction)authenticationButton;


@end

