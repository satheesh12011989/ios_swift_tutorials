//
//  AppDelegate.m
//  TouchID
//
//  Created by Satheesh  on 8/12/14.
//  Copyright (c) 2014 Satheesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

