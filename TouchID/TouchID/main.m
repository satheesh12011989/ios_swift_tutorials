//
//  main.m
//  TouchID
//
//  Created by Satheesh  on 8/12/14.
//  Copyright (c) 2014 Satheesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
