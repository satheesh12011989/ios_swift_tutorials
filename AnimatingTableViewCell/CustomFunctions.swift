//
//  CustomFunctions.swift
//  AnimatingTableViewCell
//
//  Created by Satheesh on 12/3/14.
//  Copyright (c) 2014 satheeshiOSdeveloper. All rights reserved.
//

import Foundation

protocol customFunctionsDelegate{
    func didReceiveAPIResults(results: NSDictionary)
}

class CustomFunctions: NSObject {

    var delegate : customFunctionsDelegate?
    
    class var sharedInstance: CustomFunctions {
        struct Static {
            static var instance : CustomFunctions?
            static var token : dispatch_once_t = 0
        }
        dispatch_once(&Static.token){
            Static.instance=CustomFunctions()
        }
        return Static.instance!
    }
    
    func searchItunesFor(searchTerm: String) {
        // The iTunes API wants multiple terms separated by + symbols, so replace spaces with + signs
        let itunesSearchTerm = searchTerm.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        
        // Now escape anything else that isn't URL-friendly
        if let escapedSearchTerm = itunesSearchTerm.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding) {
            let urlPath = "http://itunes.apple.com/search?term=\(escapedSearchTerm)&media=software"
            let url = NSURL(string: urlPath)
            let session = NSURLSession.sharedSession()
            
            APIService.swiftSharedInstance.getRequest(url!, withCompletion: { (data) -> Void in
                var result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                self.delegate?.didReceiveAPIResults(result)
            })
        }
    }
    
}

