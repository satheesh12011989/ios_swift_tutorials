//
//  APIService.swift
//  APIService
//
//  Created by satheesh on 12/3/14.
//  Copyright (c) 2014 satheeshiOSdeveloper. All rights reserved.
//

import UIKit

class APIService: NSObject {
    
    typealias completionHandler = (data:NSData)->Void
    
    //singleton class
    class var swiftSharedInstance: APIService {
        struct Static {
            static var instance: APIService?
            static var token: dispatch_once_t = 0
        }
        // to perform thread safe
        dispatch_once(&Static.token) {
            Static.instance = APIService()
        }
        
        return Static.instance!
    }
    
    
    //http get request using nsurlsession
    func getRequest(url:NSURL, withCompletion: completionHandler){
        let sessionConfiguration:NSURLSessionConfiguration = NSURLSessionConfiguration .defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: sessionConfiguration)
        let dataTask:NSURLSessionDataTask = session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
            
            if(error != nil){
                println(error.localizedDescription)
            }
            else{
                if(data != nil){
                    var result:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error:nil) as NSDictionary
                    
                    println(result)
                    
                    //add the method to the main thread
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        withCompletion(data: data)
                    })
                }
            }
        })
        dataTask.resume()
    }
    
}
